# Avenue Code .Net/C# Challenge#


### Purpose ###

* The purpose of this project is to simulate the available routes between any number of towns and their distances. 
  This project is made of by one side by a RESTful Web API service whose purpose is to compute the total distance of any given route, compute all available routes from any given pair of towns within a maximum of stops, and, to compute the shortest distance between any pair of towns.
* Version: 1.1

### Summary ###

* Summary of set up:
  This project is made of by one side by a RESTful Web API service  (folder WebApiRouteMap) whose purpose is to compute the total distance of any given route, compute all available routes from any given pair of towns within a maximum number of stops, and, to compute the shortest distance between any pair of towns.
  On the other side is an MVC4 app (folder MvcRouteMap) that consumes the service. 
  
* Configuration: Once the API service is running the Client App can be deployed and runned.
  The WebApiRouteMap is the service. It has the following functionalities: compute the total distance of any given route, compute all available routes from any given pair of towns within a maximum number of stops, and, to compute the shortest distance between any pair of towns.
  The MvcRouteMap is the Client App. In order to use this app and the functionalities given by the service, the user of the this Client App will see in the index page 4 options: Compute the total distance of any given route (recursivey and not recursively), another, Compute the shortest distance between any pair of towns and the last, to compute all available routes from any given pair of towns within a maximum number of stops.
  The user all has to do is insert two parameters when he/she wants to know the total distance of any given route (recursivey and not recursively), or the shortest distance for a given route: one, the directed graph, second, the route. When he/she wants to know all available routes from any given pair of towns within a maximum number of stops, all he/she has to do is enter three parameters: the directed graph, the route and the maximum number of stops.
  The Input directed graph will be in the form of a string: AB5, BC4, CD8, DC8, AD5, DE6, AE7, EB3, CE2
  This string must always have a coma as separator and a space between them (AB5, BC4)
  The Route will be introduced as a string like this: A-B-C-D, meaning that this route is composed by the edges: AB, BC, CD.
  The user just has to introduce these parameters and click on the button to request for the functionality.
  
* Dependencies:
  Visual Studio .NET 2012 .NET, Framework 4.5, Visual Studio Unit Test Framework
* Database configuration:
  NONE
* How to run tests:
  The tests are on the service project, separated for this only purpose using the Visual Studio Unit Test framework. They are all very self explanatories.
* Deployment instructions:
  The Web API service can be deploy on a server different from the MVC4 App. In my case i had them all in one PC, so i had the service hosted on http://localhost:46397/
  and the Client App, in this case, an MVC4 app just called via Http the services on the Web API. If the Web API service is going to be hosted on a server different from
  the Client app, the admin just hace to change the line of code number 15 on the web.config of the MVC4 app (<add key="RouteMapServiceURI" value="http://localhost:46397/" />) 
  and put there the server name or IP address:Port where this service will be serving its functions.
  For example, if the service will be hosted on a server called AppsServer, the admin would have to change the line of code
  <add key="RouteMapServiceURI" value="http://localhost:46397/" /> for this one: <add key="RouteMapServiceURI" value="http://AppsServer/" />

Thank you very much.

Raisel Noguera Martell