﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiRouteMap.Models;

namespace WebApiRouteMap.Tests.Controllers
{
    [TestClass]
    public class MapServiceTest
    {
        [TestMethod]
        public void MapServiceGetDistanceRecursivelyForABCDRouteShouldReturnValue17()
        {
            var mapService = new MapService();
            string inputGraph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";
            string route = "A-B-C-D";
            MapResponse result = mapService.ComputeTotalDistanceRecursively(inputGraph, route);
            Assert.AreEqual(17, result.Value);
        }

        [TestMethod]
        public void MapServiceGetDistanceRecursivelyForABCRouteShouldReturnValue9()
        {
            var mapService = new MapService();
            string inputGraph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";
            string route = "A-B-C";
            MapResponse result = mapService.ComputeTotalDistanceRecursively(inputGraph, route);
            Assert.AreEqual(9, result.Value);
        }

        [TestMethod]
        public void MapServiceGetDistanceRecursivelyForEBCRouteShouldReturnValue7()
        {
            var mapService = new MapService();
            string inputGraph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";
            string route = "E-B-C";
            MapResponse result = mapService.ComputeTotalDistanceRecursively(inputGraph, route);
            Assert.AreEqual(7, result.Value);
        }

        [TestMethod]
        public void MapServiceGetRoutesBetweenTownATownCWith2MaximumNumberOfStopsShouldReturnValueABCandValueADC()
        {
            var mapService = new MapService();
            string inputGraph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";
            string route = "A-C";
            int maximumNumberOfStops = 2;
            IEnumerable<MapResponse> result = mapService.GetRoutesWithMaxStops(inputGraph, route, maximumNumberOfStops);
            Assert.AreEqual("ABC", result.First().NodeCollection.First().Name + result.First().NodeCollection.ElementAt(1).Name + result.First().NodeCollection.ElementAt(2).Name);
            Assert.AreEqual("ADC", result.ElementAt(1).NodeCollection.First().Name + result.ElementAt(1).NodeCollection.ElementAt(1).Name + result.ElementAt(1).NodeCollection.ElementAt(2).Name);
        }

        [TestMethod]
        public void MapServiceGetRoutesBetweenTownATownDWith3MaximumNumberOfStopsShouldReturnValueABCDandValueAD()
        {
            var mapService = new MapService();
            string inputGraph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";
            string route = "A-D";
            int maximumNumberOfStops = 3;
            IEnumerable<MapResponse> result = mapService.GetRoutesWithMaxStops(inputGraph, route, maximumNumberOfStops);
            Assert.AreEqual("ABCD", result.First().NodeCollection.First().Name + result.First().NodeCollection.ElementAt(1).Name + result.First().NodeCollection.ElementAt(2).Name + result.First().NodeCollection.ElementAt(3).Name);
            Assert.AreEqual("AD", result.ElementAt(1).NodeCollection.First().Name + result.ElementAt(1).NodeCollection.ElementAt(1).Name);
        }

        [TestMethod]
        public void MapServiceGetShortestRouteBetweenTownATownCShouldReturnValue9()
        {
            var mapService = new MapService();
            string inputGraph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";
            string route = "A-C";
            MapResponse result = mapService.ComputeShortestRoute(inputGraph, route);
            Assert.AreEqual(9, result.Value);
        }

        [TestMethod]
        public void MapServiceGetShortestRouteBetweenTownBTownDShouldReturnValue12()
        {
            var mapService = new MapService();
            string inputGraph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";
            string route = "B-D";
            MapResponse result = mapService.ComputeShortestRoute(inputGraph, route);
            Assert.AreEqual(12, result.Value);
        }

        [TestMethod]
        public void MapServiceGetTotalDistanceNotRecursivelyForEBCDERouteShouldReturnValue21()
        {
            var mapService = new MapService();
            string inputGraph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";
            string route = "E-B-C-D-E";
            MapResponse result = mapService.ComputeTotalDistance(inputGraph, route);
            Assert.AreEqual(21, result.Value);
        }

        [TestMethod]
        public void MapServiceGetTotalDistanceNotRecursivelyForADEBCDERouteShouldReturnValue32()
        {
            var mapService = new MapService();
            string inputGraph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";
            string route = "A-D-E-B-C-D-E";
            MapResponse result = mapService.ComputeTotalDistance(inputGraph, route);
            Assert.AreEqual(32, result.Value);
        }
    }
}
