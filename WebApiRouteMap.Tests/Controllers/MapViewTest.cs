﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiRouteMap.Models;

namespace WebApiRouteMap.Tests.Controllers
{
    [TestClass]
    public  class MapViewTest
    {
        [TestMethod]
        public void MapViewVerifyMapCreationShouldReturnNotNull()
        {
            IMapCreator mapCreator = new MapCreator();
            string inputGraph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";
            IMapView map = mapCreator.CreateMapView(inputGraph);
            Assert.IsNotNull(map);
        }

        [TestMethod]
        public void MapViewVerifyMapCreationShouldReturn5ElementsInTheNodeCollectionPropertyOfTheMap()
        {
            IMapCreator mapCreator = new MapCreator();
            string inputGraph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";
            IMapView map = mapCreator.CreateMapView(inputGraph);
            Assert.AreEqual(5, ((MapView)map).NodeCollection.Count());
        }

        [TestMethod]
        public void MapViewVerifyMapCreationShouldReturn9ElementsInTheInputGraphCollectionPropertyOfTheMap()
        {
            IMapCreator mapCreator = new MapCreator();
            string inputGraph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";
            IMapView map = mapCreator.CreateMapView(inputGraph);
            Assert.AreEqual(9, ((MapView)map).InputGraphCollection.Count());
        }

        [TestMethod]
        public void MapViewVerifyMapCreationShouldReturnNotNullForTheNeighborCollectionPropertyOfTheMap()
        {
            IMapCreator map = new MapCreator();
            string inputGraph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";
            IMapView result = map.CreateMapView(inputGraph);
            Assert.IsNotNull(((MapView)result).NeighborCollection);
        }

        [TestMethod]
        public void MapViewVerifyMapCreationShouldReturnWeightOf5BetweenTownATownBQueryingTheNeighborCollectionPropertyOfTheMap()
        {
            IMapCreator map = new MapCreator();
            string inputGraph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";
            IMapView result = map.CreateMapView(inputGraph);
            Assert.AreEqual(5, ((MapView)result).NeighborCollection.First().NeighboringNodesCollection.First().Weight);
        }

        [TestMethod]
        public void MapViewVerifyMapCreationShouldReturnWeightOf7BetweenTownATownDQueryingTheNeighborCollectionPropertyOfTheMap()
        {
            IMapCreator map = new MapCreator();
            string inputGraph = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";
            IMapView result = map.CreateMapView(inputGraph);
            Assert.AreEqual(7, ((MapView)result).NeighborCollection.First().NeighboringNodesCollection.ElementAt(2).Weight);
        }
    }
}
