﻿using MvcRouteMap.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MvcRouteMap.Controllers
{
    public class RouteMapController : Controller
    {
        public async Task<ActionResult> RouteTotalDistanceRecursively(string inputGraph, string route)
        {
            var mapService = new MapService();
            var result = await mapService.GetRouteTotalDistanceRecursivelyAsync(inputGraph, route);
            if (Request.IsAjaxRequest())
                return PartialView("_pwRouteTotalDistanceRecursevly", result);
            else
                return View(result);
        }

        public async Task<ActionResult> ShortestRouteBetweenTwoTowns(string inputGraph, string route)
        {
            var mapService = new MapService();
            var result = await mapService.GetShortestRouteBetweenTwoTownsAsync(inputGraph, route);
            if (Request.IsAjaxRequest())
                return PartialView("_pwShortestRouteBetweenTwoTowns", result);
            else
                return View(result);
        }

        public async Task<ActionResult> RoutesWithMaxStops(string inputGraph, string route, string maxStops)
        {
            var mapService = new MapService();
            var result = await mapService.GetRoutesWithMaxStopsAsync(inputGraph, route, int.Parse(maxStops));
            if (Request.IsAjaxRequest())
                return PartialView("_pwRoutesWithMaxStops", result);
            else
                return View(result);
        }
        
        public async Task<ActionResult> RouteTotalDistanceNotRecursively(string inputGraph, string route)
        {
            var mapService = new MapService();
            var result = await mapService.GetRouteTotalDistanceAsync(inputGraph, route);
            if (Request.IsAjaxRequest())
                return PartialView("_pwRouteTotalDistanceNotRecursevly", result);
            else
                return View(result);
        }

    }
}
