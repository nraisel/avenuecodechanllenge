﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace MvcRouteMap.Models
{
    public class ServiceUriColector
    {
        public static string getServiceUri(string srv, string inputGraph, string route)
        {
            return ConfigurationManager.AppSettings["RouteMapServiceURI"] + "api/" + srv + "/" + inputGraph + "/" + route;
        }

        public static string getServiceUri(string srv, string inputGraph, string route, int maxStops)
        {
            return ConfigurationManager.AppSettings["RouteMapServiceURI"] + "api/" + srv + "/" + inputGraph + "/" + route + "/" + maxStops;
        }
    }
}