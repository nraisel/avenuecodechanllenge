﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcRouteMap.Models
{
    public class MapResponse
    {
        public IEnumerable<MapNode> NodeCollection { get; set; }
        public int Value { get; set; }
        public string Path { get; set; }
        public int MaximumNumberOfStops { get; set; }
    }
}