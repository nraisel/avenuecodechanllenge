﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace MvcRouteMap.Models
{
    public class MapService
    {
        public async Task<MapResponse> GetRouteTotalDistanceRecursivelyAsync(string inputGraph,
                                                                             string route,
                                                                             CancellationToken cancelToken = default(CancellationToken))
        {
            var uri = ServiceUriColector.getServiceUri("TotalDistanceRecursivelyForGivenRoute", inputGraph, route);
            using (HttpClient httpClient = new HttpClient())
            {
                var response = await httpClient.GetAsync(uri, cancelToken);
                return (await response.Content.ReadAsAsync<MapResponse>());
            }
        }

        public async Task<MapResponse> GetRouteTotalDistanceAsync(string inputGraph,
                                                                  string route,
                                                                  CancellationToken cancelToken = default(CancellationToken))
        {
            var uri = ServiceUriColector.getServiceUri("TotalDistanceNotRecursivelyForGivenRoute", inputGraph, route);
            using (HttpClient httpClient = new HttpClient())
            {
                var response = await httpClient.GetAsync(uri, cancelToken);
                return (await response.Content.ReadAsAsync<MapResponse>());
            }
        }

        public async Task<MapResponse> GetShortestRouteBetweenTwoTownsAsync(string inputGraph,
                                                                            string route,
                                                                            CancellationToken cancelToken = default(CancellationToken))
        {
            var uri = ServiceUriColector.getServiceUri("ShortestRouteBetweenTwoTowns", inputGraph, route);
            using (HttpClient httpClient = new HttpClient())
            {
                var response = await httpClient.GetAsync(uri, cancelToken);
                return (await response.Content.ReadAsAsync<MapResponse>());
            }
        }

        public async Task<IEnumerable<MapResponse>> GetRoutesWithMaxStopsAsync(string inputGraph,
                                                                               string route,
                                                                               int maxStops,
                                                                               CancellationToken cancelToken = default(CancellationToken))
        {
            var uri = ServiceUriColector.getServiceUri("RoutesWithMaxStops", inputGraph, route, maxStops);
            using (HttpClient httpClient = new HttpClient())
            {
                var response = await httpClient.GetAsync(uri, cancelToken);
                return (await response.Content.ReadAsAsync<IEnumerable<MapResponse>>());
            }
        }
    }
}