﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcRouteMap.Models
{
    public class MapNode
    {
        public string Name { get; set; }
        public bool Visited { get; set; }
    }
}