﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiRouteMap.Models
{
    public class MapView : IMapView
    {
        private IEnumerable<MapNode> nodeCollection;
        private IEnumerable<MapRoute> neighborCollection;
        private IEnumerable<string> inputGraphCollection;

        public MapView()
        { }

        public MapView(IEnumerable<MapNode> nodeCollection, IEnumerable<MapRoute> neighborCollection, IEnumerable<string> inputGraphCollection)
        {
            NodeCollection = nodeCollection;
            NeighborCollection = neighborCollection;
            InputGraphCollection = inputGraphCollection;
        }

        public IEnumerable<MapNode> GetNodeCollection(object inputGraph)
        {
            var result = new List<MapNode>();
            var edgeCollection = GetInputGraphCollection(inputGraph);
            foreach (var item in edgeCollection)
            {
                string edge = item.Trim().Substring(0, 2);
                string originNodeName = edge.Substring(0, 1);
                string destinationNodeName = edge.Substring(1, 1);
                var originNode = new MapNode { Name = originNodeName };
                var destinationNode = new MapNode { Name = destinationNodeName };
                if (!(result.Any(n => n.Name == originNodeName)))
                    result.Add(originNode);
                if (!(result.Any(n => n.Name == destinationNodeName)))
                    result.Add(destinationNode);
            }
            return result;
        }

        public IEnumerable<MapRoute> GetNeighborCollection(object inputGraph)
        {
            var result = new List<MapRoute>();
            var edgeCollection = GetInputGraphCollection(inputGraph);
            var nodeCollection = GetNodeCollection(inputGraph);
            var neighboringNodesCollection = new List<MapEdge>();
            foreach (var node in nodeCollection)
            {
                neighboringNodesCollection.Clear();
                foreach (var edge in edgeCollection)
                {
                    string ed = edge.Trim();
                    if (ed.Substring(0, 1) == ((MapNode)node).Name)
                    {
                        string destinationNodeName = ed.Substring(1, 1);
                        var destinationNode = new MapNode { Name = destinationNodeName };
                        int weight = int.Parse(ed.Substring(2, ed.Length - 2));
                        var edgeObj = new MapEdge { DestinationNode = destinationNode, Weight = weight };
                        neighboringNodesCollection.Add(edgeObj);
                    }
                }
                var neihgboredTownsCollection = new List<MapEdge>();
                foreach (var item in neighboringNodesCollection)
                    neihgboredTownsCollection.Add(item);
                var route = new MapRoute { OriginNode = node, NeighboringNodesCollection = neihgboredTownsCollection };
                result.Add(route);
            }
            return result;
        }

        public IEnumerable<string> GetInputGraphCollection(object inputGraph)
        {
            return ((string)inputGraph).Replace(", ", ",").Split(',');
        }

        public IEnumerable<MapNode> NodeCollection
        {
            get { return nodeCollection; }
            set { nodeCollection = value; }
        }

        public IEnumerable<MapRoute> NeighborCollection
        {
            get { return neighborCollection; }
            set { neighborCollection = value; }
        }

        public IEnumerable<string> InputGraphCollection
        {
            get { return inputGraphCollection; }
            set { inputGraphCollection = value; }
        }
    }
}