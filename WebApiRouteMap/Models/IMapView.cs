﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApiRouteMap.Models
{
    public interface IMapView
    {
        IEnumerable<MapNode> GetNodeCollection(object inputGraph);
        IEnumerable<MapRoute> GetNeighborCollection(object inputGraph);
        IEnumerable<string> GetInputGraphCollection(object inputGraph);
    }
}
