﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiRouteMap.Models
{
    public class MapFunctionalityResolver
    {
        private MapView mapView;

        public MapFunctionalityResolver(MapView mapView)
        {
            MapView = mapView;
        }

        public bool ExistEdge(string edge)
        {
            return MapView.InputGraphCollection.Any(e => e.Substring(0, 2) == edge);
        }

        public MapResponse ComputeTotalDistance(string route)
        {
            int distance = 0;
            var nodeCollection = GetNodesInRoute(route);
            var nodesNameInPairCollection = GetNodesNameInPairCollection(route);
            foreach (var nodesNameInPair in nodesNameInPairCollection)
            {
                if (MapView.InputGraphCollection.Any(e => e.Substring(0, 2) == nodesNameInPair))
                {
                    int i = 0;
                    bool found = false;

                    while (i <= MapView.InputGraphCollection.Count() - 1 && !found)
                    {
                        string edgesOfRoute = MapView.InputGraphCollection.ElementAt(i).Trim().Substring(0, 2);
                        if (nodesNameInPair == edgesOfRoute)
                        {
                            distance += int.Parse(MapView.InputGraphCollection.ElementAt(i).Trim().Substring(2, MapView.InputGraphCollection.ElementAt(i).Trim().Length - 2));
                            found = true;
                        }
                        i++;
                    }
                }
                else
                    return new MapResponse{ NodeCollection = new List<MapNode>() , Value = 0 };
            }

            return new MapResponse { NodeCollection = nodeCollection, Value = distance };
        }

        private IEnumerable<MapNode> GetNodesInRoute(string route)
        {
            var nodeCollection = new List<MapNode>();
            for (int i = 0; i <= route.Length - 1; i++) {
                string nodeName = route.Substring(i, 1);
                var mapNode = new MapNode { Name = nodeName, Visited = false };
                nodeCollection.Add(mapNode);
            }
            return nodeCollection;
        }

        private IEnumerable<string> GetNodesNameInPairCollection(string route)
        {
            var result = new List<string>();
            string formatedRoute = route.Replace("-", "");
            formatedRoute = formatedRoute.Trim();
            for (int i = 0; i <= formatedRoute.Length - 2; i++)
            {
                string origin = formatedRoute.ElementAt(i).ToString();
                string destiny = formatedRoute.ElementAt(i + 1).ToString();
                string edge = origin + destiny;
                result.Add(edge);
            }
            return result;
        }

        public MapResponse ComputeTotalDistanceRecursively(string route, IList<MapNode> nodeCollection, int distance, int pos)
        {
            if (pos <= route.Length - 2)
            {
                string originNodeName = route.Substring(pos, 1);
                var originNode = new MapNode { Name = originNodeName };
                if (!nodeCollection.Any(nn => nn.Name == originNode.Name))
                    nodeCollection.Add(originNode);
                string destinationNodeName = route.Substring(pos + 1, 1);
                var destinationNode = new MapNode { Name = destinationNodeName };
                nodeCollection.Add(destinationNode);
                if (ExistEdge(originNodeName + destinationNodeName))
                {
                    distance += MapView.NeighborCollection.Where(tn => tn.OriginNode.Name == originNodeName)
                                                          .First()
                                                          .NeighboringNodesCollection
                                                          .Where(lon => lon.DestinationNode.Name == destinationNodeName)
                                                          .First()
                                                          .Weight;
                    pos++;
                    return ComputeTotalDistanceRecursively(route, nodeCollection, distance, pos);
                }
                else
                {
                    pos = route.Length + 100;
                    distance = 0;
                    nodeCollection.Clear();
                }
            }
            return new MapResponse { NodeCollection = nodeCollection, Value = distance };
        }

        public MapResponse ComputeShortestRoute(string route, IList<MapNode> nodeCollection, string originalRoute, string path, string current, int distance, IList<MapResponse> mapAnswerList)
        {
            string originNodeName = route.Substring(0, 1);
            var originNode = new MapNode { Name = originNodeName };
            nodeCollection.Add(originNode);
            if (current != route.Substring(1, 1))
            {
                var nodeNonVisitedByOriginNode = MapView.NeighborCollection.Where(tn => tn.OriginNode.Name == originNodeName)
                                                                           .First()
                                                                           .NeighboringNodesCollection
                                                                           .Where(lon => lon.DestinationNode.Visited == false)
                                                                           .FirstOrDefault();
                if (nodeNonVisitedByOriginNode == null)
                {
                    if (mapAnswerList.Count > 0)
                        return mapAnswerList.OrderBy(g => g.Value).First();
                    else{
                        nodeCollection.Clear();
                        return new MapResponse { NodeCollection = nodeCollection, Value = 0 };
                    }
                }
                current = nodeNonVisitedByOriginNode.DestinationNode.Name;
                distance += nodeNonVisitedByOriginNode.Weight;
                if (MapView.NeighborCollection.Where(tn => tn.OriginNode.Name == originNodeName)
                                              .First()
                                              .NeighboringNodesCollection
                                              .Count() > 1)
                {
                    if (current != route.Substring(1, 1) |
                        (current == route.Substring(1, 1) && mapAnswerList.Any(e => e.NodeCollection.Any(en => en.Name.Contains(route)))))
                    {

                        MapView.NeighborCollection.Where(tn => tn.OriginNode.Name == originNodeName)
                                                  .First()
                                                  .NeighboringNodesCollection
                                                  .Where(lon => lon.DestinationNode.Name == current)
                                                  .First()
                                                  .DestinationNode.Visited = true;
                    }
                }
                path = path + "-" + originNodeName;
                route = route.Substring(0, 1).Replace(originNodeName, current);
                route = route + originalRoute.Substring(1, 1);
                return ComputeShortestRoute(route, nodeCollection, originalRoute, path, current, distance, mapAnswerList);
            }
            route = path + "-" + originalRoute.Substring(1, 1);
            if (mapAnswerList.Any(e => e.Path == route.Replace("-", "")))
                return mapAnswerList.OrderBy(g => g.Value).First();
            else
            {
                mapAnswerList.Add(new MapResponse { NodeCollection = nodeCollection, Value = distance, Path = route.Replace("-", "") });
                current = ""; 
                path = "";
                route = originalRoute;
                distance = 0;
                return ComputeShortestRoute(route, new List<MapNode>(), originalRoute, path, current, distance, mapAnswerList);
            }
        }

        public IEnumerable<MapResponse> GetRoutesWithMaxStops(int maximumNumberOfStops,
                                                              string route,
                                                              IList<MapNode> nodeCollection,
                                                              string originalRoute,
                                                              string path,
                                                              bool gotOut,
                                                              int originalMaximumNumberOfStops,
                                                              IList<MapResponse> mapAnswerList)
        {
            string originNodeName = route.Substring(0, 1);
            var originNode = new MapNode { Name = originNodeName };
            nodeCollection.Add(originNode);
            if ((originNodeName != route.Substring(2, 1)) && maximumNumberOfStops > 0)
            {
                string newOrigin = MapView.NeighborCollection.Where(tn => tn.OriginNode.Name == originNodeName)
                                                             .First()
                                                             .NeighboringNodesCollection
                                                             .Where(lon => lon.DestinationNode.Visited == false)
                                                             .First()
                                                             .DestinationNode
                                                             .Name;
                if (!gotOut)
                {
                    MapView.NeighborCollection.Where(tn => tn.OriginNode.Name == originNodeName)
                                              .First()
                                              .NeighboringNodesCollection
                                              .Where(lon => lon.DestinationNode.Name == newOrigin)
                                              .First()
                                              .DestinationNode.Visited = true;

                }
                maximumNumberOfStops--;
                route = route.Replace(originNodeName, newOrigin);
                gotOut = true;
                path = path + "-" + originNodeName;
                return GetRoutesWithMaxStops(maximumNumberOfStops, route, nodeCollection, originalRoute, path, gotOut, originalMaximumNumberOfStops, mapAnswerList);
            }
            if (originNodeName == route.Substring(route.Length - 1))
            {
                var mapAnswer = new MapResponse { NodeCollection = nodeCollection, Value = (originalMaximumNumberOfStops - maximumNumberOfStops), MaximumNumberOfStops = originalMaximumNumberOfStops };
                mapAnswerList.Add(mapAnswer);
            }

            int nodeNeighborsVisited = MapView.NeighborCollection.Where(tn => tn.OriginNode.Name == originalRoute.Substring(0, 1))
                                                                 .First()
                                                                 .NeighboringNodesCollection
                                                                 .Where(lon => lon.DestinationNode.Visited == true)
                                                                 .Count();
            int totalOfNodeNeighbors = MapView.NeighborCollection.Where(tn => tn.OriginNode.Name == originalRoute.Substring(0, 1))
                                                                 .First()
                                                                 .NeighboringNodesCollection
                                                                 .Count();

            if (nodeNeighborsVisited < totalOfNodeNeighbors)
            {
                path = "";
                gotOut = false;
                maximumNumberOfStops = originalMaximumNumberOfStops;
                route = originalRoute;
                return GetRoutesWithMaxStops(maximumNumberOfStops, route, new List<MapNode>(), originalRoute, path, gotOut, originalMaximumNumberOfStops, mapAnswerList);
            }

            return mapAnswerList;
        }

        public MapView MapView
        {
            get { return mapView; }
            set { mapView = value; }
        }
    }
}