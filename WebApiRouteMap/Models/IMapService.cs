﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApiRouteMap.Models
{
    public interface IMapService
    {
        MapResponse ComputeTotalDistance(object inputGraph, object route);
        MapResponse ComputeTotalDistanceRecursively(object inputGraph, object route);
        MapResponse ComputeShortestRoute(object inputGraph, object route);
        IEnumerable<MapResponse> GetRoutesWithMaxStops(object inputGraph, object route, int maximumNumberOfStops);
    }
}
