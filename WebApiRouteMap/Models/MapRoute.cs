﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiRouteMap.Models
{
    public class MapRoute
    {
        public MapNode OriginNode { get; set; }
        public IEnumerable<MapEdge> NeighboringNodesCollection { get; set; }
    }
}