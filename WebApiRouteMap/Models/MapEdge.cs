﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiRouteMap.Models
{
    public class MapEdge
    {
        public int Weight { get; set; }
        public MapNode DestinationNode { get; set; }
    }
}