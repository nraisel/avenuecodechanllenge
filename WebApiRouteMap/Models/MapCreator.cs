﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiRouteMap.Models
{
    public class MapCreator : IMapCreator
    {
        public IMapView CreateMapView(object input)
        {
            var mapView = new MapView();
            mapView.NodeCollection = mapView.GetNodeCollection(input);
            mapView.NeighborCollection = mapView.GetNeighborCollection(input);
            mapView.InputGraphCollection = mapView.GetInputGraphCollection(input);
            return mapView;
        }
    }
}