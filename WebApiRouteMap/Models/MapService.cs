﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiRouteMap.Models
{
    public class MapService : IMapService
    {
        public MapResponse ComputeTotalDistance(object inputGraph, object route)
        {
            var map = new MapCreator();
            var mapView = map.CreateMapView((string)inputGraph);
            var mapFunctionalityResolver = new MapFunctionalityResolver((MapView)mapView);
            return mapFunctionalityResolver.ComputeTotalDistance(((string)route).Replace("-", ""));
        }

        public MapResponse ComputeTotalDistanceRecursively(object inputGraph, object route)
        {
            var nodeCollection = new List<MapNode>();
            var map = new MapCreator();
            var mapView = map.CreateMapView((string)inputGraph);
            var mapFunctionalityResolver = new MapFunctionalityResolver((MapView)mapView);
            return mapFunctionalityResolver.ComputeTotalDistanceRecursively(((string)route).Replace("-", ""), 
                                                                            nodeCollection, 
                                                                            0, 
                                                                            0);
        }

        public MapResponse ComputeShortestRoute(object inputGraph, object route)
        {
            var nodeCollection = new List<MapNode>();
            var map = new MapCreator();
            var mapView = map.CreateMapView((string)inputGraph);
            var mapFunctionalityResolver = new MapFunctionalityResolver((MapView)mapView);
            var mapAnswerCollection = new List<MapResponse>();
            return mapFunctionalityResolver.ComputeShortestRoute(((string)route).Replace("-", ""),
                                                                 nodeCollection,
                                                                 ((string)route).Replace("-", ""),
                                                                 "", 
                                                                 "", 
                                                                 0,
                                                                 mapAnswerCollection);
        }

        public IEnumerable<MapResponse> GetRoutesWithMaxStops(object inputGraph, object route, int maximumNumberOfStops)
        {
            var nodeCollection = new List<MapNode>();
            var map = new MapCreator();
            var mapView = map.CreateMapView((string)inputGraph);
            var mapFunctionalityResolver = new MapFunctionalityResolver((MapView)mapView);
            var mapAnswerCollection = new List<MapResponse>();
            return mapFunctionalityResolver.GetRoutesWithMaxStops(maximumNumberOfStops,
                                                                  ((string)route),  
                                                                  nodeCollection,
                                                                  ((string)route),
                                                                  "",
                                                                  false,
                                                                  maximumNumberOfStops,
                                                                  mapAnswerCollection);
        }
    }
}