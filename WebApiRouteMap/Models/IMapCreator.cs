﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApiRouteMap.Models
{
    public interface IMapCreator
    {
        IMapView CreateMapView(object input);
    }
}
