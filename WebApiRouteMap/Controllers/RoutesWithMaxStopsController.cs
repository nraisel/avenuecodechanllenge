﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiRouteMap.Models;

namespace WebApiRouteMap.Controllers
{
    public class RoutesWithMaxStopsController : ApiController
    {
        public IEnumerable<MapResponse> GetRoutesWithMaxStops(string inputGraph, string route, int maximumNumberOfStops)
        {
            IMapService mapService = new MapService();
            return mapService.GetRoutesWithMaxStops(inputGraph, route, maximumNumberOfStops);
        }
    }
}
