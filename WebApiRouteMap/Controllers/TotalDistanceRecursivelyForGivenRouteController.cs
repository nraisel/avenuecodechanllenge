﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiRouteMap.Models;

namespace WebApiRouteMap.Controllers
{
    public class TotalDistanceRecursivelyForGivenRouteController : ApiController
    {
        public MapResponse GetTotalDistance(string inputGraph, string route)
        {
            IMapService mapService = new MapService();
            return mapService.ComputeTotalDistanceRecursively(inputGraph, route);
        }
    }
}
